# Vagrant Setup for G

## What is this
This project has been created to deliver some tasks requested by some company. This project is extremely complex - so for example it doesn't focus on Vagrant's security - none of boxes uses another key than default "insecure", none of users has been created or there is not so much added into security - excluding basic firewall configuration.

## Requirements
To run this project you will:
- require python or bash installed on yours laptop to trigger script
- laptop must have access to internet resources (git, vagrant images, apt)
- vagrant must be installed with virtualbox as provider
- vagrant uses 32 bit images so it should be able to run on older machines as well

## Provisioning Vagrant
To start initial provisioning run one of the following scripts located in vagrant-provisioning folder:
```
bash ./vagrant-provisioning/init.sh
```
```
python ./vagrant-provisioning/init.py
```
Both does same tasks, and generally are very simple but their task is to check if Vagrant boxes for Ansible (master) and LDAP (opendj) servers exists and are running. If not then script takes an action to build these servers, using ubuntu/trusty32 image from vagrant official repository. After process of servers deployment git and ansible are installed on Ansible (master) server plus this repository is cloned into vagrant home folder. Finally script shows parameters to connect into Vagrant Ansible instance. Please be aware that usage command 'vagrant ssh ansible' must be done from directory where Vagrant has been provisioned (/path/to/this/repo/vagrant). As mentioned - there was no action taken regarding to security as this environment is built only for testing purposes so default vagrant password is vagrant

- When deployment ends you can use ssh to login into server - with details provided at the end of script or go into vagrant directory and run:
```
vagrant ssh ansible
``` 

## Provisioning OpenDJ
There is a ansible role already written used but highly adjusted to deploy OpenDJ. This is because:
- Role was failing when service was already started
- There was no import or ldif data task
- OpenDJ doesn't support LDAPPublicKey by default and scheme has to be added
Also some of variables has been added:
- Provision with correct baseDN
- Encrypt password
- Import data enforement
- Correct url to OpenDJ 3.0.0
Data.ldif issues
- Data ldif by default was missing of one Organization Unit - general. This is added manually into data.ldif file now. Without that there was no possibility to import users from this OU

When you are logged in into ansible instance you simply will need to do couple of steps to get infrastructure working
- Deploy OpenDJ:

```
cd vagrantsetupforG
ansible-playbook opendj --ask-vault-pass
```

Because password for OpenDJ Directory Manager has been encrypted with ansible-vault so you will need to trigger playbook with --ask-vault-pass option. This will prompt you for a password to decrypt files containg password and other one with data.ldif. Because this is deom only so password is - password

When this is done OpenDJ server will be running on ldap server. You can check if ldap server is running by typing 

```
curl ldap://10.10.0.30/
```

To query server in simplier way you will need to follow another steps:
- Install ldap-utils on Ansible (master) server

```
ansible-playbook ldap-utils.yml
```

- After this to finally get list of users from specific group type:

```
ansible-playbook ldap-query.yml --ask-vault-pass -e group_query=GROUP
```

- Again you will be prompted for password to decrypt vault - where Directory Manager password sits - this is simple - password
- GROUP is group which you simply want to query - heros, villans or maybe admin?

# At the end
Please remember I created this repository, all playbooks and scripts - except on role opendj - by myself - all within 2.5 working days on weekend between some time with my family - so this is really not the 'complex' solution. Python and bash scripts both are written on simpliest possible way and are not using too much ifs, loops, functions or external modules. That means i.e. that every command is mady next to each other with simple copy-paste and every change is made by new ssh connection - this absolutely can be written in better way to make this script more complex and quicker to resolve. Also some tasks on playbooks could be more generic to be used in future - like installer - which i modified couple of times and from simple task to install openjdk only now install every ppa repository and any bunch of software. Also these roles works with debian based system only - same as vagrant boxes. And finally - vagrant spins these servers with only 512MB RAM - so you will need to give to everythin some time 

- Final result from Ansible (master) box:

```
vagrant@ansible:~/vagrantsetupforG/ansible$ ansible-playbook ldap-query.yml --ask-vault-pass -e group_query=admin
Vault password: 

PLAY [query-ldap] **************************************************************

TASK [setup] *******************************************************************
ok: [10.10.0.20]

TASK [query-ldap : Copy script] ************************************************
changed: [10.10.0.20]

TASK [query-ldap : Query all users of specific group] **************************
changed: [10.10.0.20]

TASK [query-ldap : Print All users from group] *********************************
ok: [10.10.0.20] => (item=srogers) => {
    "item": "srogers", 
    "msg": "srogers"
}
ok: [10.10.0.20] => (item=jjones) => {
    "item": "jjones", 
    "msg": "jjones"
}

PLAY RECAP *********************************************************************
10.10.0.20                 : ok=4    changed=2    unreachable=0    failed=0   

```

# Thank you
