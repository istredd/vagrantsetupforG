#!/bin/bash

# Check if vagrant is installed
if !(which vagrant>/dev/null); then
  echo "Vagrant not installed, please verify"
  exit 1
# Check if Vagrantfile config exists
elif ! [[ -a './vagrant/Vagrantfile' ]]; then
  echo "Vagrant config not exists. Please verify"
  exit 1
fi

cd vagrant
# Checks if Vagrant is already hosting both instances (ansible and ldap)
if [ $(vagrant status | grep -e ansible -e ldap | awk '{print $2}' | grep -c running) != 2 ]; then
  echo "Provisioning Vagrant"
# Build Vagrant setup if not exists
  vagrant up
else
  echo "Vagrant already running"
fi

if [ ! -d ./.vagrant/ansiblekey/ ]; then
  mkdir -p ./.vagrant/ansiblekey
  ssh-keygen -b 2048 -t rsa -f ./.vagrant/ansiblekey/id_rsa -q -N ""
fi

# Add ansible official repository, and install git with ansible
vagrant ssh ansible -c "sudo apt-get install -y software-properties-common"
vagrant ssh ansible -c "sudo apt-add-repository -y ppa:ansible/ansible"
vagrant ssh ansible -c "sudo apt-get update"
vagrant ssh ansible -c "sudo apt install -y git ansible"
# Clone this repository to Ansibe (master) server - if not exists
vagrant ssh ansible -c "ls ~/vagrantsetupforG/.git/description || git clone https://gitlab.com/istredd/vagrantsetupforG"
# Add private and public keys for Ansible master
cat .vagrant/ansiblekey/id_rsa | vagrant ssh ansible -c 'cat > $HOME/.ssh/id_rsa && chmod 600 $HOME/.ssh/id_rsa'
cat .vagrant/ansiblekey/id_rsa.pub | vagrant ssh ansible -c 'cat >> $HOME/.ssh/authorized_keys'
cat .vagrant/ansiblekey/id_rsa.pub | vagrant ssh ldap -c 'cat >> $HOME/.ssh/authorized_keys'
# Pull configuration from Vagrant
params=(`vagrant ssh-config ansible | grep -e HostName -e User -e Port | awk '{print $2}'`)

# Print information how to ssh into Ansible
echo "Vagrant provisioning done please login to server with parameters:"
echo "Usage: ssh ${params[1]}@${params[0]} -p ${params[2]}"
echo "or: vagrant ssh ansible"
