#!/usr/bin/python

import os,sys,subprocess,pdb

# Check if Vagrant is installed on machine
try:
    print("Checking if vagrant is installed...")
    subprocess.call(["vagrant", "-v"])
except OSError:
    print("No vagrant installed. Please verify")
    sys.exit(1)

# Check if Vagrantfile exists
if not os.path.exists('./vagrant/Vagrantfile'):
    print("No Vagrantfile available. Please verify")
    sys.exit(1)

# Initialization of Vagrant setup for both boxes
print("Starting up Vagrant...")
os.chdir("./vagrant")
subprocess.call(["vagrant", "up"])

if not os.path.isdir('./.vagrant/ansiblekey'):
    os.mkdir('./.vagrant/ansiblekey')
    subprocess.call(["ssh-keygen -b 2048 -t rsa -f ./.vagrant/ansiblekey/id_rsa -q -N \"\""], shell=True)

print("Ansible provisioning...")
# Add ansible official repository, and install git with ansible
subprocess.call(["vagrant ssh ansible -c \"sudo apt-get install -y software-properties-common\""], shell=True)
subprocess.call(["vagrant ssh ansible -c \"sudo apt-add-repository -y ppa:ansible/ansible\""], shell=True)
subprocess.call(["vagrant ssh ansible -c \"sudo apt-get update\""], shell=True)
subprocess.call(["vagrant ssh ansible -c \"sudo apt install -y ansible git\""], shell=True)
# Clone this repository to Ansibe (master) server
subprocess.call(["vagrant ssh ansible -c \"git clone https://gitlab.com/istredd/vagrantsetupforG\""], shell=True)
# Add private and public keys for Ansible master
subprocess.call(["cat .vagrant/ansiblekey/id_rsa | vagrant ssh ansible -c \'cat > $HOME/.ssh/id_rsa && chmod 600 $HOME/.ssh/id_rsa\'"], shell=True)
subprocess.call(["cat .vagrant/ansiblekey/id_rsa.pub | vagrant ssh ansible -c \'cat >> $HOME/.ssh/authorized_keys\'"], shell=True)
subprocess.call(["cat .vagrant/ansiblekey/id_rsa.pub | vagrant ssh ldap -c \'cat >> $HOME/.ssh/authorized_keys\'"], shell=True)
# Pull ssh configuration set for this build
params = subprocess.check_output(["vagrant", "ssh-config", "ansible"]).splitlines()
ip = str(params[1]).split(" ")[3]
port = str(params[3]).split(" ")[3]
user = str(params[2]).split(" ")[3]

# Print information how to ssh into Ansible
print("Vagrant provisioning done please login to server with parameters:")
print("Usage: ssh", ip[:-1], "-l", user[:-1], "-p", port[:-1])
print("or: vagrant ssh ansible")
